import pandas as pd
import re
def removeComments(inputFileName, outputFileName):
    input = open(inputFileName, "r")
    linenr = 1
    headerlist = []
    output = open(outputFileName, "w")
    #output.write(input.readline())
    for line in input:
        if not line.lstrip().startswith("#") \
        and not line.lstrip().startswith("5s"):
            line = re.sub(r"(P|A)\t*", "", line )
            line = line.replace("agency_cd	", "")
            line = line.replace("USGS	", "")
            line = re.sub(r"\d{5,}_\d{5,}_\d{5,}_cd", "	", line)
            line = re.sub(r"\d{5,}_0", "0", line)
            line = line.replace("			", "	")
            line = line.replace("		", "	")
            line = line.replace("	", ",")
            line = line[:-2]
            line=line+"\n"
            if line.lstrip().startswith("site_no"):
                headerlist.append(linenr)
            linenr += 1
            output.write(line)
    input.close()
    output.close()
removeComments("monthtest.rdb", "monthtest_remove.rdb")